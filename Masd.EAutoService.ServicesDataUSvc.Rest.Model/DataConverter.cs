﻿using Masd.EAutoService.ServicesDataUSvc.Model;

namespace Masd.EAutoService.ServicesDataUSvc.Rest.Model
{
    public static class DataConverter
    {
        public static ServiceDTO ConvertToServiceData(this Service service)
        {
            return new ServiceDTO { ServiceId = service.ServiceId, Name = service.Name, Price = service.Price };
        }

    }
}
