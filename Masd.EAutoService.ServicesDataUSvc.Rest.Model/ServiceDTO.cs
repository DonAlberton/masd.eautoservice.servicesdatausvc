﻿namespace Masd.EAutoService.ServicesDataUSvc.Rest.Model
{
    public class ServiceDTO
    {
        public int ServiceId { get; set; }
        public string? Name { get; set; }
        public double Price { get; set; }
    }
}