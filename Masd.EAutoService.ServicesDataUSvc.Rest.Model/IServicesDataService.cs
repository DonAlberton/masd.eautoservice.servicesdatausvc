﻿namespace Masd.EAutoService.ServicesDataUSvc.Rest.Model
{
    public interface IServicesDataService
    {
        ServiceDTO GetService(int id);
        ServiceDTO[] GetServices();

    }
}
