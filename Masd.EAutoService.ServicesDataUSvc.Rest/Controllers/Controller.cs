using Microsoft.AspNetCore.Mvc;
using Masd.EAutoService.ServicesDataUSvc.Model;
using Masd.EAutoService.ServicesDataUSvc.Rest.Model;
using Masd.EAutoService.ServicesDataUSvc.Logic;

namespace Masd.EAutoService.ServicesDataUSvc.Rest.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ServiceDataController : ControllerBase, IServicesDataService
    {
        private readonly ILogger<ServiceDataController> logger;
        private readonly IServicesData servicesData;


        public ServiceDataController(ILogger<ServiceDataController> logger)
        {
            this.logger = logger;
            this.servicesData = new ServicesData();
        }

        [HttpGet]
        [Route("GetService")]
        public ServiceDTO GetService(int id)
        {
            Service filteredServices = this.servicesData.GetService(id);
            return this.servicesData.GetService(id).ConvertToServiceData();
        }


        [HttpGet]
        [Route("GetServices")]
        public ServiceDTO[] GetServices()
        {
            Service[] services = this.servicesData.GetServices();
            return services.Select(service => service.ConvertToServiceData()).ToArray();
        }
    }
}
