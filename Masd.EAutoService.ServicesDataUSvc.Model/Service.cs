﻿namespace Masd.EAutoService.ServicesDataUSvc.Model
{
    public class Service
    {
        public int ServiceId { get; set; }
        public string Name { get; private set; }
        public double Price { get; private set; }

        public Service(int serviceId, string name, double price)
        {
            this.ServiceId = serviceId;
            this.Name = name;
            this.Price = price;
        }

        public virtual string GetDescription()
        {
            return string.Format("{0} cost: {1}", this.Name, this.Price);
        }
    }
}