﻿namespace Masd.EAutoService.ServicesDataUSvc.Model
{
    public interface IServicesData
    {
        Service GetService(int id);
        Service[] GetServices();
    }
}
