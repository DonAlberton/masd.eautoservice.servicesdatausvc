﻿using System.Text.Json;
using Masd.EAutoService.ServicesDataUSvc.Rest.Model;

namespace Masd.EAutoService.ServicesDataUSvc.Rest.Client
{
    public class ServicesDataServiceClient : IServicesDataService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        private static int port = 8083;
        private static string protocol = "http";
        public ServiceDTO GetService(int id)
        {
            string webServiceUrl = $"{protocol}://localhost:{port}/ServiceData/GetService?id={id}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            ServiceDTO servicesData = ConvertJson1(jsonResponseContent);

            return servicesData;
        }

        public ServiceDTO[] GetServices()
        {
            string webServiceUrl = $"{protocol}://localhost:{port}/ServiceData/GetServices";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            ServiceDTO[] servicesData = ConvertJson(jsonResponseContent);

            return servicesData;
        }

        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }
        private ServiceDTO[] ConvertJson(string json)
        {
            JsonSerializerOptions options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            ServiceDTO[] servicesData = JsonSerializer.Deserialize<ServiceDTO[]>(json, options);


            return servicesData;
        }
        private ServiceDTO ConvertJson1(string json)
        {
            JsonSerializerOptions options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            ServiceDTO servicesData = JsonSerializer.Deserialize<ServiceDTO>(json, options);


            return servicesData;
        }

    }

}
