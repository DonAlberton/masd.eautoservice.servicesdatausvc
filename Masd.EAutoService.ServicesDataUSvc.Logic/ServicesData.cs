﻿using Masd.EAutoService.ServicesDataUSvc.Model;

namespace Masd.EAutoService.ServicesDataUSvc.Logic
{
    public class ServicesData : IServicesData
    {
        private static readonly Service[] Services;
        private static readonly object serviceDataLock = new object();
        private const string servicesFilePath = "services.json";

        static ServicesData()
        {
            lock (serviceDataLock)
            {
                Service[] services = ServicesFileHandler.ReadJson(servicesFilePath);
                Services = services;
            }
        }
        public ServicesData() { }

        public Service GetService(int id)
        {
            lock (serviceDataLock)
            {
                return ServicesFileHandler.ReadJson(servicesFilePath).FirstOrDefault(m => m.ServiceId == id)!;
            }
        }
        


        public Service[] GetServices()
        {
            lock (serviceDataLock)
            {
                return Services;
            }
        }
    }
}