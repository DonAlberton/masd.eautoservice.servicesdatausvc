﻿using Masd.EAutoService.ServicesDataUSvc.Model;
using System.Text.Json;

namespace Masd.EAutoService.ServicesDataUSvc.Logic
{
    public class ServicesFileHandler
    {
        public static Service[] ReadJson(string fileName)
        {
            Service[] service = JsonSerializer.Deserialize<Service[]>(File.ReadAllText(fileName));
            return service;
        }

    }
}
